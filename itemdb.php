<?php

  include_once(dirname(__FILE__) . "/PHPQuery/PHPQuery.php");
  phpQuery::plugin("WebBrowser");


  class itemDB {

     static $items = array();

     static function init(){
        phpQuery::$defaultCharset = 'UTF-8';

        $url = 'http://kvartus.ru/xml/yrl_new_all.xml';

        phpQuery::ajaxAllowURL( $url );
        phpQuery::browserGet($url, new CallbackParameterToReference($browser));
          if($browser){
             foreach($browser->find("offer") as $offer){
                $offer = pq( $offer );
                self::$items[ $offer->attr("internal-id") ] = 
                        self::prepareStr( $offer->find("description")->text() );
             };
          };      
     }

     static function prepareStr($string){
         return md5( substr( preg_replace("/[\\n\\r\\.\\,\\s]+/","",
                                             $string ), 0, 75) );
     }

     static function check($string){
        foreach(self::$items as $id => $item ){
            if(self::prepareStr($string) == $item){
                return $id;
            };
        };

        return false;
     }
  }

  itemDB::init();
