<?php

  include_once(dirname(__FILE__) . "/PHPQuery/PHPQuery.php");
  phpQuery::plugin("WebBrowser");

  abstract class Parser {

    protected $visited_urls;
    protected $urls;
    protected $callbackParse;
    protected $result;

    function __construct( ){
       $this->visited_urls = array();
       $this->urls = array();
       $this->callbackParse = new Callback(array($this,'parse'), new CallbackParam );
       $this->result = array();
    }


    abstract public function parse($brwser);
//       $this->filterItem($item);

    abstract protected function parseNext($brwser);
//       $this->addUrl($next_url);

    protected function addUrl($url){
       if(is_array($url)){
          foreach($url as $c_url){
             $this->addUrl($c_url);
          };
       }elseif( is_string($url) ){
         if(in_array($url,$this->visited_urls)) return;
         if(in_array($url,$this->urls)) return;

         array_push($this->urls, $url);
       }
    }

    public function execute($url = null){
       if($url) $this->addUrl($url);

       while( $url = array_shift( $this->urls)){
          $this->start($url);
       }
    }

    protected function start($url){
         if(in_array($url,$this->visited_urls)) return;

         phpQuery::ajaxAllowURL( $url );

         try{
           phpQuery::browserGet($url, $this->callbackParse);
         }catch(Zend_Http_Client_Exception $e){
           print "ERROR: " . $e->getMessage() . "\n";
         };

         array_push($this->visited_urls, $url );
    }

    protected function filterItem($item){
       array_push($this->result, $item);
    }
  }