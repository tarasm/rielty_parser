<?php

  include_once(dirname(__FILE__) . "/parser.php");
  include_once(dirname(__FILE__) . "/itemdb.php");

  class Kvartirydomazemlya extends Parser{

    function __construct( ){
       phpQuery::$defaultCharset = 'UTF-8';

       parent::__construct();

       $this->addUrl('http://kvartirydomazemlya.ru/ru/realestateagent-1828/realestateagentproperties.htm');
    }


    public function parse($browser){
       foreach($browser->find("#ctl15__tableOffer tr td.cellModuleSpecialOffer_GroupTypeLine")
                                ->parent()->next() as $item_tr){
          $item_tr = pq($item_tr);
          $item_descr = $item_tr->next()->next()->find("span.FieldName")->text();
          
          $this->filterItem(array( "url" => $item_tr->find("a.aRealEstateOfferTitle")->attr("href"),
                           "description" => $item_descr ));
      };

      $this->parseNext($browser);
    }

    protected function filterItem($item){
       if($id = itemDB::check($item["description"] )){
         $item["id"] = $id;
         parent::filterItem($item);
       }
       else{ throw new parseEmptyItemException($item["url"] . " not found"); };
    }

    protected function parseNext($browser){
       $this->addUrl($browser->find("#ctl15__hyperLinkNext")->attr("href"));
    }

  }

  class parseEmptyItemException extends Exception { }